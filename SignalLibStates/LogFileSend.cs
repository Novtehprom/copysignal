﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalLibStates
{
    public class LogFileSend
    {
        public string Name { get; set; }
        public string Created { get; set; }
        public string Size { get; set; }
    }
}
