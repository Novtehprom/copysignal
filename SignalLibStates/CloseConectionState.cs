﻿namespace SignalLibStates
{
    public enum CloseConectionState
    {
        Normal = 0,
        AlreadyConnected = 1,
        TheProfileIsMissing = 2
    }
}
