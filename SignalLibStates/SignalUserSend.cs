﻿namespace SignalLibStates
{
    public class SignalUserSend
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public UserType Type { get; set; }
        public string Group { get; set; }
        public bool IsAdmin { get; set; }
        public string Deleted { get; set; }
        public string Application { get; set; }
    }
}
