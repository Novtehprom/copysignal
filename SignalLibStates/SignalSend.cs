﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalLibStates
{
    public class SignalSend
    {
        public string Id { get; set; }
        public string NameFrom { get; set; }
        public string NameTo { get; set; }
        public string Message { get; set; }
    }
}
