﻿namespace SignalLibStates
{
    /// <summary>
    /// The result of the operation Connect()
    /// </summary>
    public enum ConnectionResult
    {
        /// <summary>
        /// The connection is made
        /// </summary>
        ConnectedOk = 0,
        /// <summary>
        /// The connection is not performed because the user name and password 
        /// is already working in the system from another computer
        /// </summary>
        AlreadyConnected = 1,
        /// <summary>
        /// Not authenticate
        /// </summary>
        TokenIsNotRecieved =2,
        /// <summary>
        /// The connection process is started but not yet completed
        /// </summary>
        NotReadyYet = 3,
        /// <summary>
        /// hubConnection.Start error
        /// </summary>
        HabStartError = 4,
        /// <summary>
        /// EnvokeConnect() error
        /// </summary>
        EnvokeConnectError = 5,
        /// <summary>
        /// Profile is not Exist And user in not Admin
        /// </summary>
        TheProfileIsMissing = 6
    }
}
