﻿namespace SignalLibStates
{
    /// <summary>
    /// The possible States of an instance of the class SignalCopier
    /// </summary>
    public enum ClientState
    {
        /// <summary>
        /// Waiting messages from the server or from the сreator
        /// </summary>
        Waiting = 0,
        /// <summary>
        /// The signal is sent to the server
        /// </summary>
        MessageIsSending = 1,
        /// <summary>
        /// Confirmation sent to the server
        /// </summary>
        ConformationIsSending = 2,
        /// <summary>
        /// The request to disconnect is sent To the server
        /// </summary>
        Disconnecting = 3,
        /// <summary>
        /// Disable done. Use for close connection process
        /// </summary>
        Disconnected = 4,
        /// <summary>
        /// Running the connection
        /// </summary>
        Connecting = 5,
        /// <summary>
        /// Running the reconnected command sending
        /// </summary>
        ReconnectedSending = 6,
    }
}
