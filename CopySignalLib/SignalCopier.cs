﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.IO;
using System.Management;
using System.Threading;
using System.Threading.Tasks;
using CopySignalLib.Model;
using SignalLibStates;
using CopySignalLib.Controller;
using System.Collections.Generic;
using NLog.Fluent;

namespace CopySignalLib
{
    /// <summary>
    /// Copy signals from Maser to Copier by SignalR technology
    /// </summary>
    public class SignalCopier: ISignalCopier
    {
        #region Constructors

        /// <summary>
        /// Creates class for copy signal from maser trader to copier trader for SignalR
        /// In this embodiment, the constructor automatically runs the Login() and token is filled in
        /// To connect use Connect() without parameters
        /// </summary>
        /// <param name="baseUrl">Base URL site for example: "http://profit4Traders.com"</param>
        /// <param name="eMail">User email</param>
        /// <param name="password">User password</param>
        /// <param name="appName">Name of your application</param>
        public SignalCopier(string baseUrl, string eMail, string password, string appName)
        {
            this.baseUrl = baseUrl;
            this.eMail = eMail;
            this.appName = appName;
            controller = new RecordBufersController();
            //LoginAsync(eMail, password, loginEnd);
            SetAppName();
            SetEventsBrokkers();
        }

        /// <summary>
        /// Creates class for copy signal from maser trader to copier trader for SignalR
        /// This version of the constructor for the case when Authentication is performed 
        /// by other means, and the token has already been obtained
        /// To connect use Connect() without parameters
        /// </summary>
        /// <param name="baseUrl">Base URL site for example: "http://profit4Traders.com"</param>
        /// <param name="token">Token from site Profit4Traders.com</param>
        /// <param name="appName">Name of your application</param>
        public SignalCopier(string baseUrl, string token, string appName)
        {
            this.baseUrl = baseUrl;
            this.appName = appName;
            this.token = token;
            controller = new RecordBufersController();
            SetAppName();
            SetEventsBrokkers();
        }

        /// <summary>
        /// Creates class for copy signal from master trader to copier trader for SignalR
        /// This version of the constructor involves Calling method 
        /// Login(string eMail, string password) that returns the authentication 
        /// result (true or false) and sets the token, and then you can call Connect()
        /// </summary>
        /// <param name="baseUrl">Base URL site for example: "http://profit4Traders.com"</param>
        /// <param name="appName">Name of your application</param>
        public SignalCopier(string baseUrl, string appName)
        {
            this.baseUrl = baseUrl;
            this.appName = appName;
            controller = new RecordBufersController();
            SetAppName();
            SetEventsBrokkers();
        }

        #endregion

        #region Options

        /// <summary>
        /// The number of delays while waiting for a connection
        /// </summary>
        private const int countConnectionWhaite = 10;

        /// <summary>
        /// The number of attempts to run Invoke
        /// </summary>
        private const int countTry = 20;

        /// <summary>
        /// TimeOut to wait for a change State  4 сек
        /// </summary>
        private const int stateWaiting = 4000;

        /// <summary>
        /// TimeOut to wait for a connection attempt 10 сек
        /// </summary>
        private const int tryWaiting = 10000;

        /// <summary>
        /// TimeOut to wait for the disconnection  2 сек
        /// </summary>
        private const int disconnecWaiting = 2000;

        /// <summary>
        /// TimeOut to to re-call the method EnvokeReconected() 4 сек
        /// </summary>
        private const int repeatReconnected = 4000;
        /// <summary>
        /// TimeOut to to re-call the method EnvokeReconected() 4 сек
        /// </summary>
        private const int repeatMReconnected = 4000;

        #endregion

        #region Local variables

        /// <summary>
        /// Instance of atomic operations Controller for multithreaded access to collections of signals
        /// </summary>
        private RecordBufersController controller;

        /// <summary>
        /// Token received from site Profit4Traders.com by Log in;
        /// </summary>
        private string token;

        /// <summary>
        /// Base Url of profit4Traders site
        /// </summary>
        private string baseUrl;

        /// <summary>
        /// User email
        /// </summary>
        private string eMail;

        /// <summary>
        /// Unique client cod 
        /// </summary>
        private string clientCod;

        /// <summary>
        /// Name of current application
        /// </summary>
        private string appName;

        /// <summary>
        /// File for PrintLog
        /// </summary>
        private StreamWriter file = null;

        /// <summary>
        /// Instance of Hub Connection for SignalR client
        /// </summary>
        public HubConnection hubConnection;

        /// <summary>
        /// Instance of Hab proxy for SignalR client
        /// </summary>
        public IHubProxy hubProxy;

        /// <summary>
        /// States of an instance of the class SignalCopier
        /// </summary>
        private ClientState clientState = ClientState.Disconnected;

        /// <summary>
        /// When the event is triggered "hubConnection.Closed" if stopConnect = false, start StartConnection()
        /// </summary>
        private bool stopConnect = false;

        /// <summary>
        /// The status of the connection process
        /// </summary>
        private ConnectionResult connectionResult;

        /// <summary>
        /// For Dispose()
        /// </summary>
        private bool disposed = false;

        #endregion

        #region Lock flags

        private object lockReseive = new object();
        private object lockSend = new object();
        private object dischargeSendLock = new object();
        private object logFileLock = new object();
        private object lockReseiveChat = new object();
        private object lockSendChat = new object();
        
        #endregion

        #region Propities

        /// <summary>
        /// Waiting: Waiting messages from the server or from the сreator
        /// MessageIsSending: The signal is sent to the server
        /// ConformationIsSending: Confirmation sent to the server
        /// Disconnecting: The request to disconnect is sent To the server
        /// Disconnected: Disable done
        /// </summary>
        public ClientState ClientState { get { return this.clientState; } }

        private SignalUserSend currentUser { get; set; }

        public SignalUserSend CurrentUser
        {
            get { return this.currentUser; }
            private set { this.currentUser = value; } 
        }

        public string Token
        {
            get { return token; }
            set { token = value; }
        }
        #endregion

        #region Event defenitions

        public delegate void ConnectionStateChange();
        public event ConnectionStateChange OnConnected;
        public event ConnectionStateChange OnDisConnected;
        public event ConnectionStateChange OnConnecting;
        public event ConnectionStateChange OnReconnecting;

        public delegate void LocalReceiveMessage(Guid messageId, string message, string userName);
        public delegate void ReceiveMessage(string message, string userName);
        public event ReceiveMessage OnReceiveMessage;

        public event ReceiveMessage OnReceiveChatMessage;

        public delegate void LocalConformation(Guid mesageId);
        public delegate void Conformation();
        public event Conformation OnConformation;

        public delegate void StopClient(CloseConectionState state);
        public event StopClient OnStopClient;

        public delegate void UserConnected(SignalUserSend signalUserSend);
        public event UserConnected OnOtherUserConnected;
        public event UserConnected OnOtherUserDisConnected;

        #endregion

        #region Events brokers

        private LocalReceiveMessage localOnReceiveMessage;
        private void LocalOnReceiveMessage(Guid messageId, string message, string userName)
        {
            lock (lockReseive)
            {
                if (controller.IsNotReceived(messageId))
                {
                    OnReceiveMessage?.Invoke(message, userName);
                    controller.AddReceiveLine(messageId);
                }
                EnvokeConfirm(messageId);
            }
        }

        private ReceiveMessage localOnReceivePrivateMessage;
        private void LocalOnReceivePrivateMessage(string message, string userNameFrom)
        {
            lock (lockReseiveChat)
            {
                OnReceiveChatMessage?.Invoke(message, userNameFrom);
            }
        }

        private LocalConformation localOnConformation;
        private void LocalOnConformation(Guid messageId)
        {
            controller.RemoveSendLine(messageId);
            OnConformation?.Invoke();
        }

        private StopClient localOnStopClient;
        private void LocalOnStopClient(CloseConectionState state)
        {
            stopConnect = true;
            try
            {
                var result = Task.Run(() => hubConnection.Stop());
                result.Wait();
                if(state == CloseConectionState.AlreadyConnected) connectionResult = ConnectionResult.AlreadyConnected;
                else if(state == CloseConectionState.TheProfileIsMissing) connectionResult = ConnectionResult.TheProfileIsMissing;
                this.clientState = ClientState.Disconnected;

            }
            catch (Exception e)
            {
                this.PrintLog("hubConnection.Stop() error: " + e.HResult + ((e.InnerException == null) ? e.Message : e.InnerException.Message));
            }

            int i = 0;
            while (hubConnection.State != ConnectionState.Disconnected && i < countTry)
            {
                i++;
                Thread.Sleep(stateWaiting);
            }
            if (i >= 10)
            {
                this.PrintLog(string.Format("hubConnection.Stop() error: {0} (Count Try = {1}", "Connection stop error Try count more then maximum Count Count Try", countTry));
            }
            OnStopClient?.Invoke(state);
        }

        private UserConnected localOnUserConnected;
        private void LocalOnUserConnected(SignalUserSend signalUserSend)
        {
            this.currentUser = signalUserSend;
            this.eMail = signalUserSend.Email;
            connectionResult = ConnectionResult.ConnectedOk;
            OnConnected?.Invoke();
        }

        private UserConnected localOnOtherUserConnected;
        private void LocalOnOtherUserConnected(SignalUserSend signalUserSend)
        {
            OnOtherUserConnected?.Invoke(signalUserSend);
        }

        private UserConnected localOnOtherUserDisConnected;
        private void LocalOnOtherUserDisConnected(SignalUserSend signalUserSend)
        {
            OnOtherUserDisConnected?.Invoke(signalUserSend);
        }
        #endregion

        #region Public methods

        /// <summary>
        /// Log in site Profit4Traders.com async
        /// </summary>
        /// <param name="eMail">User email</param>
        /// <param name="password">User password</param>
        public async Task<string> LoginAsync(string eMail, string password)
        {
            ManagerWebServerRequest managerWebServerRequest = new ManagerWebServerRequest(baseUrl);
            return await managerWebServerRequest.LoginByAspServer(eMail, password);
        }

        /// <summary>
        /// Return byte array of avatar image from asp.net server
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<byte[]> GetAvatarAsync(string token)
        {
            ManagerWebServerRequest managerWebServerRequest = new ManagerWebServerRequest(baseUrl);
            return await managerWebServerRequest.GetAvatarImage(token);
        }

        /// <summary>
        /// Return string of TradeSystem from asp.net server
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<string> GetTradeSystem(string token)
        {
            ManagerWebServerRequest managerWebServerRequest = new ManagerWebServerRequest(baseUrl);
            return await managerWebServerRequest.GetTradeSystem(token);
        }

        /// <summary>
        /// The reference to the function processing the result of the process connections
        /// </summary>
        /// <param name="result"></param>
        public delegate void ConnectionEnd(ConnectionResult result);

        /// <summary>
        /// Creates connection with SignalR server
        /// </summary>
        /// <param name="connectionEnd"></param>
        public async void Connect(ConnectionEnd connectionEnd)
        {
            connectionResult = ConnectionResult.NotReadyYet;
            if (token == null || token == "") connectionResult = ConnectionResult.TokenIsNotRecieved;
            else await StartConnection();
            int i = 0;
            while(connectionResult == ConnectionResult.NotReadyYet && i < 100 ) { Thread.Sleep(200); i++; }
            connectionEnd(connectionResult);
        }

        /// <summary>
        /// Starts the disconnect method on the SignalR server
        /// </summary>
        public void Disconnect()
        {
            bool envokeOk = false;
            this.clientState = ClientState.Disconnecting;
            int i = 0;
            do
            {
                try
                {
                    hubProxy.Invoke("closeLog");
                    hubProxy.Invoke("disconnect");
                    envokeOk = true;
                }
                catch (Exception ee)
                {
                    envokeOk = false;
                    Exception inerEx = ee;
                    while (inerEx.InnerException != null) { inerEx = inerEx.InnerException; }
                    PrintLog(string.Format("Error in Disconnect(). hubProxy.Invoke(\"disconnect\"): {0} {1} ", inerEx.HResult, inerEx.Message));
                    int j = 0;
                    do { Thread.Sleep(stateWaiting); j++; } while (hubConnection.State != ConnectionState.Connecting ||j < countConnectionWhaite);
                    i++;
                }
            } while (i < countTry && envokeOk == false);
            if (envokeOk == false)
            {
                PrintLog(string.Format("Error in Disconnect(). hubProxy.Invoke(\"disconnect\"): {0} (Count Try = {1})", "Connecting error Try count more then maximum Count Count Try", countTry));
            }
            else
            {
                int j = 0;
                while (this.clientState == ClientState.Disconnecting && j < countTry) { Thread.Sleep(disconnecWaiting); j++; }
            }

            if (file != null)
            {
                file.Close();
                file = null;
            }
        }

        /// <summary>
        /// Runs a method of sending a signal to copy on the server, SignalR
        /// </summary>
        /// <param name="message"></param>
        public async void SendSignal(string message)
        {   
            this.clientState = ClientState.MessageIsSending;
            SignalLine signalLine = new SignalLine
            {
                Id = Guid.NewGuid(),
                Message = message
            };
            controller.AddSendLine(signalLine);
            await EnvokeSendSignalMessage(signalLine);
        }

        /// <summary>
        /// Runs a method of sending a signal to copy on the server, SignalR
        /// </summary>
        /// <param name="message"></param>
        public async void SendChatMessage(string message, string userNameTo)
        {
            await Task.Run(() =>
            {
                try
                {
                    hubProxy.Invoke("sendPrivateMessage", new object[] { message, userNameTo });
                }
                catch (Exception e)
                {
                    string mess = "";
                    Exception innerEx = e;
                    while(innerEx !=null)
                    {
                        mess += " |" + innerEx.Message; 
                    }
                    PrintLog(mess);
                }
            });
        }

        /// <summary>
        /// Runs a method of sending a signal to copy on the server, SignalR
        /// </summary>
        /// <param name="message"></param>
        public async void SendChatGroupMessage(string message)
        {
            await Task.Run(() =>
            {
                try
                {
                    hubProxy.Invoke("sendGroupMessage", new object[] { message });
                }
                catch (Exception e)
                {
                    string mess = "";
                    Exception innerEx = e;
                    while (innerEx != null)
                    {
                        mess += " -> " + innerEx.Message;
                    }
                    PrintLog(mess);
                }
            });
        }

        /// <summary>
        /// Close error log file
        /// </summary>
        public void CloseLog()
        {
            if (file != null)
            {
                file.Close();
                file = null;
            }
        }

        #endregion

        #region Local methods

        /// <summary>
        /// Set unique client cod for prevent working with one account from distinct points
        /// </summary>
        private void SetAppName()
        {
            ManagementObjectSearcher systeminfo = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_OperatingSystem");
            foreach (ManagementObject queryObj in systeminfo.Get())
            {
                this.clientCod = queryObj["InstallDate"].ToString();
            }

        }

        /// <summary>
        /// Set events brokkers delegates
        /// </summary>
        private void SetEventsBrokkers()
        {
            this.localOnReceiveMessage = new LocalReceiveMessage(LocalOnReceiveMessage);
            this.localOnReceivePrivateMessage = new ReceiveMessage(LocalOnReceivePrivateMessage);
            this.localOnConformation = new LocalConformation(LocalOnConformation);
            this.localOnStopClient = new StopClient(LocalOnStopClient);
            this.localOnUserConnected = new UserConnected(LocalOnUserConnected);
            this.localOnOtherUserConnected = new UserConnected(LocalOnOtherUserConnected);
            this.localOnOtherUserDisConnected = new UserConnected(LocalOnOtherUserDisConnected);
        }

        /// <summary>
        /// Configure parameters and start the connection
        /// </summary>
        /// <returns></returns>
        private Task StartConnection()
        {
            return Task.Run(() =>
            {
                this.clientState = ClientState.Connecting;
                stopConnect = true;
                // Создаем соединение с сервером
                hubConnection = new HubConnection(this.baseUrl + "/signalr");

                // загружаем token в заголовок
                hubConnection.Headers.Add("myauthtoken", this.token);
                hubConnection.StateChanged += HubConnection_StateChanged;
                hubConnection.Reconnected += HubConnection_Reconnected;
                hubConnection.Error += HubConnection_Error;
                hubConnection.Closed += HubConnection_Closed;

                // Создаем proxy для для работы с сервером
                hubProxy = hubConnection.CreateHubProxy("signalHub");

                var context = SynchronizationContext.Current;

                hubProxy.On<Guid, string, string>("receiveSignalMessage", (messageId, message, userName) =>
                    this.localOnReceiveMessage(messageId, message, userName)
                    );

                hubProxy.On<string, string>("receivePrivateMessage", (message, userNameFrom) =>
                    this.localOnReceivePrivateMessage(message, userNameFrom)
                    );

                hubProxy.On<Guid>("confirmation", (messageId) =>
                    this.localOnConformation(messageId)
                    );

                hubProxy.On<SignalUserSend>("onUserConnected", (signalUserSend) =>
                    this.localOnUserConnected(signalUserSend)
                    );

                hubProxy.On<SignalUserSend>("onOtherUserConnected", (signalUserSend) =>
                    this.localOnOtherUserConnected(signalUserSend)
                    );

                hubProxy.On<SignalUserSend>("onOtherUserDisConnected", (signalUserSend) =>
                    this.localOnOtherUserDisConnected(signalUserSend)
                    );

                hubProxy.On<CloseConectionState>("stopClient", (state) =>
                    this.localOnStopClient(state)
                    );

                int icon = 0;
                do
                {
                    try
                    {
                        var result = Task.Run(() => hubConnection.Start());
                        result.Wait();
                    }
                    catch (Exception eConn)
                    {
                        Exception inerEx = eConn;
                        while (inerEx.InnerException != null) { inerEx = inerEx.InnerException; }
                        PrintLog(string.Format("Error in Connect(). hubConnection.Start(): {0} {1} ", inerEx.HResult, inerEx.Message));
                        // pause before the next connection attempt
                        Thread.Sleep(tryWaiting);
                    }
                    icon++;
                } while (hubConnection.State != ConnectionState.Connected && icon < countTry);
                if (icon == countTry)
                {
                    connectionResult = ConnectionResult.HabStartError;
                    clientState = ClientState.Disconnected;
                    return;
                }
                stopConnect = false;
                // registration on the server, SignalR
                Task.Run(() => EnvokeConnect());
            });
        }

        /// <summary>
        /// Check on the server
        /// </summary>
        private async void EnvokeConnect()
        {
            var diconn = false;
            try
            {
                clientState = ClientState.Connecting;
                await hubProxy.Invoke("connect", new object[] { this.appName, this.clientCod });
                clientState = ClientState.Waiting;
            }
            catch (Exception ee)
            {
                if(ee.HResult !=-2146233079 || clientState != ClientState.Disconnected)
                {
                    Exception inerEx = ee;
                    while (inerEx.InnerException != null) { inerEx = inerEx.InnerException; }
                    PrintLog(string.Format("Error in Connect(). hubProxy.Invoke(\"connect\"): {0} {1} ", inerEx.HResult, inerEx.Message));
                    stopConnect = true;
                    if (hubConnection.State == ConnectionState.Connected) Disconnect();
                }
                else
                {
                    diconn = true;
                }
            }
            if (this.clientState == ClientState.Waiting)
            {
                await DischargeSendBuffer();
                if(CurrentUser!= null && CurrentUser.Type == UserType.Copier)
                {
                    Thread.Sleep(repeatReconnected);
                    await EnvokeReconected();
                }
                Thread.Sleep(repeatMReconnected);
                await DischargeSendBuffer();
            }
            else if(diconn != true)
            {
                connectionResult = ConnectionResult.EnvokeConnectError;
                if (hubConnection.State != ConnectionState.Connected)
                {
                    Disconnect();
                }
                clientState = ClientState.Disconnected;
                connectionResult = ConnectionResult.EnvokeConnectError;
            }
        }

        /// <summary>
        /// Send a confirmation to the server about getting a signal
        /// </summary>
        /// <param name="messageId"></param>
        private async void EnvokeConfirm(Guid messageId)
        {
            int i = 0;
            this.clientState = ClientState.ConformationIsSending;
            while (this.clientState != ClientState.Waiting && i < countTry)
            {
                try
                {
                    int j = 0;
                    while (hubConnection.State != ConnectionState.Connected && j < countConnectionWhaite) { Thread.Sleep(stateWaiting); j++; }
                    await hubProxy.Invoke("confirmMessageReceive", new object[] { messageId });
                    this.clientState = ClientState.Waiting;
                }
                catch (Exception ee)
                {
                    Exception inerEx = ee;
                    while (inerEx.InnerException != null) { inerEx = inerEx.InnerException; }
                    PrintLog(string.Format("Error in LocalOnReceiveMessage. EnvokeConfirm(Guid messageId): {0} {1} ", inerEx.HResult, inerEx.Message));
                    int j = 0;
                    while (hubConnection.State != ConnectionState.Connected && j < countConnectionWhaite) { Thread.Sleep(stateWaiting); j++; }
                    if(hubConnection.State != ConnectionState.Connected)
                    {
                        PrintLog(string.Format("When sending the conformation failed to restore the broken connection. The number of attempts is exhausted. The maximum number of attempts = {0}", countConnectionWhaite));
                    }
                }
                i++;
            }
            if(this.clientState != ClientState.Waiting)
            {
                PrintLog(string.Format("Failed to send conformation. The number of attempts is exhausted. The maximum number of attempts= {0}", countTry));
                    this.clientState = ClientState.Waiting;
            }
        }

        /// <summary>
        /// Send a signal to the server
        /// </summary>
        /// <param name="signalLine"></param>
        private Task EnvokeSendSignalMessage(SignalLine signalLine)
        {
            return Task.Run(() =>
            {
                lock (lockSend)
                {
                    int i = 0;
                    this.clientState = ClientState.MessageIsSending;
                    while (this.clientState != ClientState.Waiting && i < countTry)
                    {
                        try
                        {
                            hubProxy.Invoke("sendSignalMessage", new object[] { signalLine.Id, signalLine.Message });
                            this.clientState = ClientState.Waiting;
                        }
                        catch (Exception ee)
                        {
                            Exception inerEx = ee;
                            while (inerEx.InnerException != null) { inerEx = inerEx.InnerException; }
                            PrintLog(string.Format("Error in EnvokeSendSignalMessage(SignalLine signalLine): {0} {1} ", inerEx.HResult, inerEx.Message));
                            int j = 0;
                            while (hubConnection.State != ConnectionState.Connected && j < countConnectionWhaite) { Thread.Sleep(stateWaiting); j++; }
                            if (hubConnection.State != ConnectionState.Connected)
                            {
                                PrintLog(string.Format("When sending the confirmation failed to restore the broken connection. The number of attempts is exhausted. The maximum number of attempts = {0}", countConnectionWhaite));
                            }
                        }
                    }
                    if (this.clientState != ClientState.Waiting)
                    {
                        PrintLog(string.Format("Failed to send signal. The number of attempts is exhausted. The maximum number of attempts= {0}", countTry));
                        this.clientState = ClientState.Waiting;
                    }
                }
            });
        }

        /// <summary>
        /// Send a reconnected command to the server
        /// </summary>
        /// <returns></returns>
        private Task EnvokeReconected()
        {
            return Task.Run(() =>
            {
                lock (lockSend)
                {
                    int i = 0;
                    this.clientState = ClientState.ReconnectedSending;
                    while (this.clientState != ClientState.Waiting && i < countTry)
                    {
                        try
                        {
                            hubProxy.Invoke("Reconnected");
                            this.clientState = ClientState.Waiting;
                        }
                        catch (Exception ee)
                        {
                            Exception inerEx = ee;
                            while (inerEx.InnerException != null) { inerEx = inerEx.InnerException; }
                            PrintLog(string.Format("Error in EnvokeReconected(): {0} {1} ", inerEx.HResult, inerEx.Message));
                            int j = 0;
                            while (hubConnection.State != ConnectionState.Connected && j < countConnectionWhaite) { Thread.Sleep(stateWaiting); j++; }
                            if (hubConnection.State != ConnectionState.Connected)
                            {
                                PrintLog(string.Format("When sending the EnvokeReconected command failed to restore the broken connection. The number of attempts is exhausted. The maximum number of attempts = {0}", countConnectionWhaite));
                            }
                        }
                    }
                    if (this.clientState != ClientState.Waiting)
                    {
                        PrintLog(string.Format("Failed to send EnvokeReconected command. The number of attempts is exhausted. The maximum number of attempts= {0}", countTry));
                        this.clientState = ClientState.Waiting;
                    }
                }
            });
        }

        /// <summary>
        /// After the connection is restored, send all the sent and unconfirmed messages again
        /// </summary>
        /// <returns></returns>
        private Task DischargeSendBuffer()
        {
            return Task.Run(() =>
            {
                lock(dischargeSendLock)
                {
                    List<SignalLine> sendLines = controller.GetSendLines();
                    foreach (SignalLine l in sendLines)
                    {
                        EnvokeSendSignalMessage(l);
                    }
                }
            });
        }

        /// <summary>
        /// If the disconnect alarm to start the connection
        /// </summary>
        private void HubConnection_Closed()
        {
            if (stopConnect == false)
            {
                Task.Run(() => StartConnection());
            }
            else
            {
                this.clientState = ClientState.Disconnected;
            }
        }

        /// <summary>
        /// Print log messages to file
        /// </summary>
        /// <param name="errmessage"></param>
        private void PrintLog(string errmessage)
        {
            lock (logFileLock)
            {
                Log.Instance.Info(errmessage);
            }
        }

        /// <summary>
        /// Print log messages to file
        /// </summary>
        /// <param name="errmessage"></param>
        private void PrintLog(Exception e, string errmessage)
        {
            lock (logFileLock)
            {
                Log.Instance.Error(e, string.Format("Exception error message:{0}.\r\n error:{1}", errmessage, e.StackTrace));
            }
        }

        #endregion

        #region Event handling 

        /// <summary>
        /// When changing the connection status
        /// </summary>
        /// <param name="habStat"></param>
        private void HubConnection_StateChanged(StateChange habStat)
        {
            switch (habStat.NewState)
            {
                case ConnectionState.Connected:
                    break;
                case ConnectionState.Disconnected:
                    OnDisConnected?.Invoke();
                    break;
                case ConnectionState.Connecting:
                    OnConnecting?.Invoke();
                    break;
                case ConnectionState.Reconnecting:
                    OnReconnecting?.Invoke();
                    break;
            }
        }

        /// <summary>
        /// Print log of error hab
        /// </summary>
        /// <param name="e"></param>
        private void HubConnection_Error(Exception e)
        {
            Exception inerEx = e;
            string mess = inerEx.Message;
            while (inerEx.InnerException != null) { inerEx = inerEx.InnerException; mess += " -> " + inerEx.Message; }
            PrintLog(string.Format("Error in Hab: HResult:{0} message:{1}\r\n Stack Trace:{2} ", inerEx.HResult, mess, e.StackTrace ));
        }

        /// <summary>
        /// When hab connection is reconnected
        /// </summary>
        private async void HubConnection_Reconnected()
        {
            OnConnected?.Invoke();
            await DischargeSendBuffer();
            Thread.Sleep(repeatMReconnected);
            await DischargeSendBuffer();
            if (CurrentUser != null && CurrentUser.Type == UserType.Copier)
            {
                await EnvokeReconected();
                Thread.Sleep(repeatReconnected);
                await EnvokeReconected();
            }
        }

        #endregion

        /// <summary>
        /// Release resources
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    hubConnection.Dispose();
                }
                this.disposed = true;
            }
        }
    }
}
