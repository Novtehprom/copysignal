﻿using SignalLibStates;
using System;
using System.Threading.Tasks;
using static CopySignalLib.SignalCopier;

namespace CopySignalLib
{
    public interface ISignalCopier:IDisposable
    {
        #region Properties

        ClientState ClientState { get; }
        SignalUserSend CurrentUser { get; }
        string Token { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Log in site Profit4Traders.com
        /// </summary>
        /// <param name="eMail">User email</param>
        /// <param name="password">User password</param>
        Task<string> LoginAsync(string eMail, string password);

        /// <summary>
        /// Return byte array of avatar image from asp.net server
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<byte[]> GetAvatarAsync(string token);

        /// <summary>
        /// Return string of TradeSystem from asp.net server
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<string> GetTradeSystem(string token);

        /// <summary>
        /// Creates connection with SignalR server
        /// </summary>
        /// <param name="connectionEnd"></param>
        void Connect(ConnectionEnd connectionEnd);

        /// <summary>
        /// Starts the disconnect method on the SignalR server
        /// </summary>
        void Disconnect();

        /// <summary>
        /// Runs a method of sending a signal to copy on the server, SignalR
        /// </summary>
        /// <param name="message"></param>
        void SendSignal(string message);

        /// <summary>
        /// Runs a method of sending a chat message , SignalR
        /// </summary>
        /// <param name="message"></param>
        void SendChatMessage(string message, string userNameTo);

        /// <summary>
        /// Runs a method of sending a chat message , SignalR
        /// </summary>
        /// <param name="message"></param>
        void SendChatGroupMessage(string message);

        void CloseLog();
        #endregion

        #region Event defenitions

        event ConnectionStateChange OnConnected;
        event ConnectionStateChange OnDisConnected;
        event ConnectionStateChange OnConnecting;
        event ConnectionStateChange OnReconnecting;
        event ReceiveMessage OnReceiveMessage;
        event Conformation OnConformation;
        event StopClient OnStopClient;
        event UserConnected OnOtherUserConnected;
        event UserConnected OnOtherUserDisConnected;
        event ReceiveMessage OnReceiveChatMessage;

        #endregion
    }
}
