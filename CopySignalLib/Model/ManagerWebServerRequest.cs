﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CopySignalLib.Model
{
    public class ManagerWebServerRequest
    {
        public ManagerWebServerRequest(string baseUrl)
        {
            this.baseUrl = baseUrl;
        }

        private string baseUrl;
        private string token;

        public async Task<string> LoginByAspServer(string eMail, string pass)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var pairs = new Dictionary<string, string>
                            {
                                {"username", eMail},
                                {"password", pass},
                                {"grant_type", "password"}
                            };
                var formContent = new FormUrlEncodedContent(pairs);
                var response = client.PostAsync("Token", formContent);
                await response.ConfigureAwait(false);
                if (!response.Result.IsSuccessStatusCode) return "";
                var res = response.Result.Content.ReadAsStringAsync();
                await res;
                var str = res.Result;
                dynamic result = JsonConvert.DeserializeObject(str);
                token = result.access_token;
                return token;
            }
        }

        public async Task<byte[]> GetAvatarImage(string token)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl + "api/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var response = client.GetByteArrayAsync("profile/avatar");
                await response;
                var result = response.Result;
                return result;
            }
        }

        public async Task<string> GetTradeSystem(string token)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl + "api/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var response = client.GetStringAsync("profile/tradesystem");
                await response;
                var result = response.Result;
                return result;
            }
        }
    }
}
