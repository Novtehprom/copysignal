﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopySignalLib.Model
{
    public class SignalLine
    {
        public Guid Id { get; set; }
        public string Message { get; set; }
    }
}
