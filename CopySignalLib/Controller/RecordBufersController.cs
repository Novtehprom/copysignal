﻿using System;
using System.Collections.Generic;
using System.Linq;
using CopySignalLib.Model;

namespace CopySignalLib.Controller
{
    public class RecordBufersController
    {
        /// <summary>
        /// During normal operation contains no more than 1-2 signals
        /// </summary>
        private List<SignalLine> sendRecordCollection = new List<SignalLine>();
        /// <summary>
        /// A small charger of some (sizeReceiveBuffer) cartridges
        /// </summary>
        private List<Guid> receiveRecordCollection = new List<Guid>();

        private  object lockSendObj = new object();
        private  object lockReceiveObj = new object();

        private const int sizeReceiveBuffer = 1000;

        public  List<SignalLine> GetSendLines()
        {
            lock(lockSendObj)
            {
                lock (lockSendObj)
                {
                    List<SignalLine> reval = new List<SignalLine>();
                    foreach (SignalLine l in sendRecordCollection)
                    {
                        reval.Add(l);
                    }
                    return reval;
                }
            }
        }
        public  void AddSendLine(SignalLine line)
        {
            lock (lockSendObj)
            {
                sendRecordCollection.Add(line);
            }
        }
        public  void RemoveSendLine(Guid messageId)
        {
            lock (lockSendObj)
            {
                SignalLine line = sendRecordCollection.FirstOrDefault(l => l.Id == messageId);
                if(line!=null)sendRecordCollection.Remove(line);
            }
        }
        public  bool IsNotReceived(Guid Id)
        {
            lock (lockReceiveObj)
            {
                return receiveRecordCollection.Count(guid => guid == Id) == 0;
            }
        }
        public  void AddReceiveLine(Guid id)
        {
            lock (lockReceiveObj)
            {
                if (receiveRecordCollection.Count > sizeReceiveBuffer) receiveRecordCollection.RemoveAt(0);
                receiveRecordCollection.Add(id);
            }
        }
        public  void RemoveReceiveLine(Guid id)
        {
            lock (lockReceiveObj)
            {
                receiveRecordCollection.Remove(id);
            }
        }
    }
}
