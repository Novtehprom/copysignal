﻿using NLog;

namespace CopySignalLib
{
    internal static class Log
    {
        public static Logger Instance { get; private set; }

        static Log()
        {
            Instance = LogManager.GetCurrentClassLogger();
        }
    }
}
