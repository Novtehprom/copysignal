﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSignalLib.Domain
{
    /// <summary>
    /// The possible States of the application
    /// </summary>
    public enum AppState
    {
        /// <summary>
        /// For display connection state in form
        /// </summary>
        Connected = 0,
        /// <summary>
        /// For display connection state in form
        /// </summary>
        Disconnected = 1,
        /// <summary>
        /// For display connection state in form
        /// </summary>
        Connecting = 2,
        /// <summary>
        /// For display connection state in form
        /// </summary>
        ReConnecting = 3,
        /// <summary>
        /// Is set when the event fires OnStopClient instance of the class SignalCopier 
        /// </summary>
        ConnectionClosed = 4
    }
}
