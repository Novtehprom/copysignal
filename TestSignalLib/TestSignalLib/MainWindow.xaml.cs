﻿using CopySignalLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TestSignalLib.Domain;
using TestSignalLib;
using System.Threading;
using System.Collections.ObjectModel;
using SignalLibStates;

namespace TestSignalLib
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //this.copier = new SignalCopier(@"http://Profit4Traders.com", "men2040@gmail.com", "123123", "TestSignalLib");
            this.copier = new SignalCopier(@"http://profit4traders.fdns.uk", "TestSignalLib");
            //this.copier = new SignalCopier(@"http://localhost:53131", "TestSignalLib");
            //this.copier = new SignalCopier(@"http://localhost:53131", "men2040@gmail.com", "123123", "TestSignalLib");
            copier.OnConnected += Copier_OnConnected;
            copier.OnConnecting += Copier_OnConnecting;
            copier.OnReconnecting += Copier_OnReconnecting;
            copier.OnDisConnected += Copier_OnDisConnected;
            copier.OnStopClient += Copier_OnStopClient;
            copier.OnReceiveMessage += Copier_OnReceiveMessage;
            copier.OnOtherUserConnected += Copier_OnOtherUserConnected;
            this.listMessages.ItemsSource = this.messages;
        }

        private void Copier_OnOtherUserConnected(SignalUserSend signalUserSend)
        {
            throw new NotImplementedException();
        }

        private ObservableCollection<string> messages = new ObservableCollection<string>();
        private int countRecord = 0;

        private void Copier_OnReceiveMessage(string message, string userName)
        {
            copier.SendSignal(message);
            Dispatcher.Invoke(() =>
            {
                this.messages.Add("From " + userName + ": " + message);
                this.countRecord++;
                this.sentNumberTextBox.Text = countRecord.ToString();
            });
        }

        /// <summary>
        /// An instance of the class copying the signals of traders on the technology of SignalR
        /// </summary>
        private ISignalCopier copier;

        /// <summary>
        /// The status of this application
        /// </summary>
        private AppState appState = AppState.Disconnected;

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            int i = 0;
            switch (appState)
            {
                case AppState.Disconnected:
                    if(copier.Login(this.email.Text, this.password.Text)) copier.Connect((ConnectionResult result) => 
                    {
                        switch (result)
                        {
                            case (ConnectionResult.ConnectedOk):
                                this.connectionResult.Text = "Ok";
                                break;
                            case (ConnectionResult.AlreadyConnected):
                                this.connectionResult.Text = "AlreadyConnected";
                                this.appState = AppState.Disconnected;
                                break;
                            case (ConnectionResult.HabStartError):
                                this.connectionResult.Text = "HabStartError";
                                this.appState = AppState.Disconnected;
                                break;
                            case (ConnectionResult.EnvokeConnectError):
                                this.connectionResult.Text = "EnvokeConnectError";
                                this.appState = AppState.Disconnected;
                                break;
                        }
                    });
                    break;
                case AppState.Connected:
                    copier.Disconnect();
                    i = 0;
                    while (this.appState != AppState.ConnectionClosed && i < 10)
                    {
                        Thread.Sleep(1000);
                        i++;
                    }
                    this.appState = AppState.Disconnected;
                    break;
                default:
                    copier.Disconnect();
                    i = 0;
                    while (this.appState != AppState.ConnectionClosed && i < 10)
                    {
                        Thread.Sleep(1000);
                        i++;
                    }
                    this.appState = AppState.Disconnected;
                    break;
            }
        }

        private delegate void SetConnectionState(string text, Color color);

        private void Copier_OnDisConnected()
        {
            appState = AppState.Disconnected;
            Dispatcher.BeginInvoke(new SetConnectionState(SetConnectionStateMethod), new object[] { "Disconnected", Color.FromRgb(255, 18, 0) });
        }

        private void Copier_OnReconnecting()
        {
            appState = AppState.ReConnecting;
            Dispatcher.BeginInvoke(new SetConnectionState(SetConnectionStateMethod), new object[] { "Reconnecting", Color.FromRgb(255, 246, 0) });
        }

        private void Copier_OnConnecting()
        {
            appState = AppState.Connecting;
            Dispatcher.BeginInvoke(new SetConnectionState(SetConnectionStateMethod), new object[] { "Connecting", Color.FromRgb(255, 168, 0) });
        }

        private void Copier_OnConnected()
        {
            appState = AppState.Connecting;
            Dispatcher.BeginInvoke(new SetConnectionState(SetConnectionStateMethod), new object[] { "Connected", Color.FromRgb(2, 199, 20)} );
        }

        private void Copier_OnStopClient(SignalLibStates.CloseConectionState state)
        {
            this.appState = AppState.ConnectionClosed;
        }

        private void SetConnectionStateMethod(string text, Color color)
        {
            SolidColorBrush mySolidColorBrush = new SolidColorBrush();
            this.stateTextBlock.Text = text;
            mySolidColorBrush.Color = color;
            this.elipseConnected.Fill = mySolidColorBrush;
            switch (text)
            {
                case "Connected":
                    this.loginButton.Content = "Disconnect";
                    this.userName.Text = this.copier.CurrentUser.Name;
                    this.group.Text = this.copier.CurrentUser.Group;
                    this.eMail.Text = this.copier.CurrentUser.Email;
                    this.typeOfUser.Text = ((this.copier.CurrentUser.Type == SignalLibStates.UserType.Master) ? "Master" : "Copier") + ((this.copier.CurrentUser.IsAdmin) ? " is Admin" : "");
                    this.email.IsReadOnly = true;
                    this.password.IsReadOnly = true;
                    if(copier.CurrentUser.Type == SignalLibStates.UserType.Master)
                    {
                        this.numberLbl.Visibility = Visibility.Visible;
                        this.numberTextBox.Visibility = Visibility.Visible;
                        this.numberButton.Visibility = Visibility.Visible;
                        this.sentNumberLbl.Content = "Number of sent messages:";
                    }
                    else
                    {
                        this.numberLbl.Visibility = Visibility.Hidden;
                        this.numberTextBox.Visibility = Visibility.Hidden;
                        this.numberButton.Visibility = Visibility.Hidden;
                        this.sentNumberLbl.Content = "Number of recieved messages:";
                    }
                    this.runPanel.Visibility = Visibility.Visible;
                    break;
                case "Connecting":
                    this.loginButton.Content = "Disconnect";
                    break;
                case "Reconnecting":
                    this.loginButton.Content = "Disconnect";
                    break;
                case "Disconnected":
                    this.loginButton.Content = "Connect";
                    this.email.IsReadOnly = false;
                    this.password.IsReadOnly = false;
                    this.runPanel.Visibility = Visibility.Hidden;
                    break;
            }
        }

        private async void RunButton_Click(object sender, RoutedEventArgs e)
        {
            await RunSend();
        }

        private Task RunSend()
        {
            int number = 0;
            int.TryParse(this.numberTextBox.Text, out number);
            return Task.Run(() =>
            {
                for (int j = 0; j < number; j++)
                {
                    string mess = "5;Close Position;05/14/2016 11:04:21 AM;ES;BuyToCover;Market;2080.5;0;1;Close;6_2016;27e9be89796f4a1498debd5706ffcdee;Sim101;1;0.01%;1;44880.11;-850;" + (countRecord + 1).ToString() + ";1;Short";
                    copier.SendSignal(mess);
                    Dispatcher.Invoke(() =>
                    {
                        ShowMess(mess);
                    });
                    mess = "5;Close Position;05/14/2016 11:04:21 AM;ES;BuyToCover;Market;2080.5;0;1;Close;6_2016;27e9be89796f4a1498debd5706ffcdee;Sim101;1;0.01%;1;44880.11;-850;" + (countRecord + 1).ToString() + ";2;Short";
                    copier.SendSignal(mess);
                    Dispatcher.Invoke(() =>
                    {
                        ShowMess(mess);
                    });
                    Thread.Sleep(1000);
                }
            });
        }

        private void ShowMess(string mess)
        {
            this.messages.Add(mess);
            this.countRecord++;
            this.sentNumberTextBox.Text = countRecord.ToString();
        }

        private void ExitButtonButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if(this.copier.ClientState != SignalLibStates.ClientState.Disconnected)
            {
                this.copier.Disconnect();
                int i = 0;
                while (this.appState != AppState.ConnectionClosed && i < 10)
                {
                    Thread.Sleep(1000);
                    i++;
                }
            }
        }

        private void reSetButton_Click(object sender, RoutedEventArgs e)
        {
            this.countRecord = 0;
            this.messages.Clear();
            this.sentNumberTextBox.Text = countRecord.ToString();
            copier.CloseLog();
        }
    }
}
